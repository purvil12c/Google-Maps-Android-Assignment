package purvilbambharolia.locationtracker;

import java.util.ArrayList;
import java.util.List;

import purvilbambharolia.locationtracker.LocationPoint;

/**
 * Created by purvil12c on 09/03/18.
 */

public class SessionTracking {

    private String sessionName;
    private List<LocationPoint> locationPoints = new ArrayList<>();
    private double distanceTravelled = -1;
    private double averageSpeed = -1;

    public SessionTracking(String sessionName){
        this.sessionName = sessionName;
    }

    public String getSessionName(){
        return this.sessionName;
    }

    public List<LocationPoint> getLocationPoints(){
        return this.locationPoints;
    }

    public void addLocationPoint(LocationPoint locationPoint){
        this.locationPoints.add(locationPoint);
    }

    public double getDistanceTravelled(){
        if(this.distanceTravelled!=-1)  return this.distanceTravelled;

        double tempDistanceTravelled = 0;
        for(int i = 0;i<locationPoints.size()-1;i++){
            tempDistanceTravelled+=distance(locationPoints.get(i).getLatitude(), locationPoints.get(i+1).getLatitude(), locationPoints.get(i).getLongitude(), locationPoints.get(i+1).getLongitude());
        }
        this.distanceTravelled = tempDistanceTravelled;

        return this.distanceTravelled;
    }

    public double getAverageSpeed(){
        if(this.averageSpeed!=-1)   return this.averageSpeed;

        ArrayList<Double> avgSpeeds = new ArrayList<>();

        for(int i = 0;i<locationPoints.size()-1;i++){
            avgSpeeds.add(
                    0.06 *
                    distance(locationPoints.get(i).getLatitude(), locationPoints.get(i+1).getLatitude(), locationPoints.get(i).getLongitude(), locationPoints.get(i+1).getLongitude())/(getTimeinMins(locationPoints.get(i+1).timeinMS,locationPoints.get(i).timeinMS))
            );
        }

        double averageSpeed = 0.0;
        for(Double d: avgSpeeds){
            averageSpeed+=d;
        }
        averageSpeed = averageSpeed/avgSpeeds.size();
        if(Double.isNaN(averageSpeed) || Double.isInfinite(averageSpeed)){
            averageSpeed = 0;
        }
        return averageSpeed/avgSpeeds.size();
    }

    public double getTimeinMins(String ms1, String ms2){
        double ms = Double.parseDouble(ms1) - Double.parseDouble(ms2);
        ms = ms/(1000*60);
        if(Double.isNaN(ms) || Double.isInfinite(ms)){
            ms = 0;
        }
        return ms;
    }

    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        return distance;
    }
}
