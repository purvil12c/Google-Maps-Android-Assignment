package purvilbambharolia.locationtracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PastTrackingActivity extends AppCompatActivity  implements OnMapReadyCallback {

    List<String> spinnerArray =  new ArrayList<String>();
    List<String> spinnerArray2 =  new ArrayList<String>();
    private GoogleMap googleMapGlobal;
    public List<SessionTracking> sessionTrackings = new ArrayList<>();
    public HashMap<String, DateTracking> dateTrackingHashMap = new HashMap<>();

    /*
        Approach 1 -
        Outer HashMap Structure -
        Key: Date -----> Value: Inner HashMap -
                                Key: Session Start Time ------> Value: List of LocationPoints

        Approach 2 -
        HashMap of Date, DateTracking Object
        DateTracking ---> Date, List<SessionTracking>
        SessionTracking ---> sessionName, List<LocationPoints>

        Two Drop Downs - 1. ---> Date
                         2. ---> Session Time

     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_tracking);

        //Map Fragment Setup...
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getSessionTrackingData();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMapGlobal = googleMap;
        googleMapGlobal.setMyLocationEnabled(false);
        /*

        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(-35.016, 143.321),
                        new LatLng(-34.747, 145.592),
                        new LatLng(-34.364, 147.891),
                        new LatLng(-33.501, 150.217),
                        new LatLng(-32.306, 149.248),
                        new LatLng(-32.491, 147.309)));


        // Position the map's camera near Alice Springs in the center of Australia,
        // and set the zoom factor so most of Australia shows on the screen.
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-23.684, 133.903), 4));

        */

    }

    private void getSessionTrackingData(){
        System.out.println("Getting Date wise Data...");
        String firebasePath = TrackingActivity.userPathGlobal;
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(firebasePath);
        System.out.println("fetching from --"+firebasePath);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                sessionTrackings = new ArrayList<>();
                spinnerArray2 =  new ArrayList<String>();
                googleMapGlobal.clear();
                dateTrackingHashMap = new HashMap<>();

                for(DataSnapshot temp: dataSnapshot.getChildren()) {
                    String trackingName = temp.getKey();
                    String date = trackingName.substring(0, 11);
                    //System.out.println("key....." + temp.getKey());
                    //System.out.println("Value...." + temp.getValue());
                    /*
                    DateTracking dateTracking;
                    if(dateTrackingHashMap.containsKey(date)){
                        dateTracking = dateTrackingHashMap.get(date);
                    }
                    else{
                        dateTracking = new DateTracking(date);
                        dateTrackingHashMap.put(date, dateTracking);
                    }
                    */

                    SessionTracking sessionTracking = new SessionTracking(trackingName);
                    for(DataSnapshot temp2: temp.getChildren()){
                        //System.out.println("key....." + temp2.getKey());
                        //System.out.println("Value...." + temp2.getValue());
                        sessionTracking.addLocationPoint(temp2.getValue(LocationPoint.class));
                        //dateTracking.addSessionTrackings();
                    }
                    sessionTrackings.add(sessionTracking);
                    //System.out.println("Average Speed "+sessionTracking.getSessionName()+"-- "+sessionTracking.getAverageSpeed()+"km/hr"+" Distance - "+sessionTracking.getDistanceTravelled());
                }
                populateDateWiseData();
                for(DateTracking dateTracking: dateTrackingHashMap.values()){
                    System.out.println("Date - "+dateTracking.getDate()+" Distance - "+dateTracking.getTotalDistanceTravelled()+" Average Speed-"+dateTracking.getAverageSpeed());
                }
                populateDropDown();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    private void populateDateWiseData(){
        for(SessionTracking sessionTracking: sessionTrackings){
            String date = sessionTracking.getSessionName().substring(0,11);
            if(!dateTrackingHashMap.containsKey(date)){
                dateTrackingHashMap.put(date, new DateTracking(date));
            }
            dateTrackingHashMap.get(date).addtoTotalDistanceTravelled(sessionTracking.getDistanceTravelled()/1000);
            dateTrackingHashMap.get(date).addAverageSpeed(sessionTracking.getAverageSpeed());
            dateTrackingHashMap.get(date).addSessionTracking(sessionTracking);
        }
    }

    private void populateDropDown(){

        for(SessionTracking sessionTracking: sessionTrackings){
            //System.out.println("sessionName -- "+sessionTracking.getSessionName());
            spinnerArray.add(sessionTracking.getSessionName());
        }
        Spinner spinner = (Spinner) findViewById(R.id.sess_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //System.out.println(adapterView.getItemAtPosition(i));
                //LocationPoint locationPoint = sessionTrackings.get(i).getLocationPoints().get(0);
                //System.out.println(locationPoint.getLatitude());

                populateGoogleMap(sessionTrackings.get(i).getLocationPoints());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*
        for(DateTracking dateTracking: dateTrackingHashMap.values()){
            //System.out.println("sessionName -- "+sessionTracking.getSessionName());
            spinnerArray.add(dateTracking.getDate());
        }
        Spinner spinner = (Spinner) findViewById(R.id.sess_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //System.out.println(adapterView.getItemAtPosition(i));
                //LocationPoint locationPoint = sessionTrackings.get(i).getLocationPoints().get(0);
                //System.out.println(locationPoint.getLatitude());
                String date = adapterView.getItemAtPosition(i).toString();
                for(SessionTracking sessionTracking: dateTrackingHashMap.get(date).getSessionTrackings()){
                    populateGoogleMap(sessionTracking.getLocationPoints());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        */
        for(DateTracking dateTracking: dateTrackingHashMap.values()){
            //System.out.println("sessionName -- "+sessionTracking.getSessionName());
            spinnerArray2.add(dateTracking.getDate());
        }
        Spinner spinner2 = (Spinner) findViewById(R.id.date_spinner);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray2);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //System.out.println(adapterView.getItemAtPosition(i));
                //LocationPoint locationPoint = sessionTrackings.get(i).getLocationPoints().get(0);
                //System.out.println(locationPoint.getLatitude());

                DateTracking dateTracking = dateTrackingHashMap.get(adapterView.getItemAtPosition(i).toString());
                TableLayout tblLayout = (TableLayout)findViewById(R.id.table);
                TableRow row = (TableRow)tblLayout.getChildAt(1);
                TextView tw = (TextView) row.getChildAt(0);
                NumberFormat formatter = new DecimalFormat("#0.00");
                tw.setText(""+dateTracking.getSessionTrackings().size());
                tw = (TextView) row.getChildAt(1);
                tw.setText(""+formatter.format(dateTracking.getTotalDistanceTravelled())+" Km");
                tw = (TextView) row.getChildAt(2);
                tw.setText(formatter.format(dateTracking.getAverageSpeed())+" Km/hr");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    private void populateGoogleMap(List<LocationPoint> locationPoints){
        googleMapGlobal.clear();
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.clickable(true);
        int position = 0;
        for(LocationPoint tempLocationPoint: locationPoints){
            polylineOptions.add(new LatLng(tempLocationPoint.getLatitude(), tempLocationPoint.getLongitude()));
            String title = "Tracked Position";
            if(position == 0) {
                title = "Start Position";
            }
            if(position == locationPoints.size()-1){
                title = "End Position";
            }
            googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(tempLocationPoint.getLatitude(), tempLocationPoint.getLongitude()))
                    .title(title)
                    .alpha(0)
                    .snippet("Lat - "+tempLocationPoint.getLatitude()+" Long - "+tempLocationPoint.getLongitude()+" Battery - "+tempLocationPoint.getBatteryPercentage()+"%"));

            googleMapGlobal.addCircle(new CircleOptions()
                    .center(new LatLng(tempLocationPoint.getLatitude(), tempLocationPoint.getLongitude())).fillColor(R.color.green).strokeColor(R.color.red).radius(15).visible(true));
            position+=1;
        }
        googleMapGlobal.addPolyline(polylineOptions);
        if(locationPoints.size()!=0) {
            googleMapGlobal.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationPoints.get(locationPoints.size() - 1).getLatitude(), locationPoints.get(locationPoints.size() - 1).getLongitude()), 17));
            //googleMapGlobal.addCircle(new CircleOptions().center(new LatLng(locationPoints.get(locationPoints.size() - 1).getLatitude(), locationPoints.get(locationPoints.size() - 1).getLongitude())).fillColor(R.color.colorAccent).strokeColor(R.color.colorPrimary).radius(15).visible(true));
            googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(locationPoints.get(locationPoints.size() - 1).getLatitude(), locationPoints.get(locationPoints.size() - 1).getLongitude())));
        }
        googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(locationPoints.get(0).getLatitude(), locationPoints.get(0).getLongitude())));

    }
}
