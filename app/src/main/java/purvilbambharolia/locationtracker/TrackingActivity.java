package purvilbambharolia.locationtracker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TrackingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap googleMapGlobal;

    private static List<LocationPoint> locationPoints = new ArrayList<>();

    public static String firebaseCurrentPath = "";

    private static final int MY_PERMISSIONS_REQUEST = 7867;

    Button startServiceButton;
    Button stopServiceButton;
    Intent serviceIntent;

    public static String userEmail = "";
    public static String userPathGlobal = "";

    private void signOut(){
        FirebaseAuth.getInstance().signOut();
        MainActivity.mGoogleSignInClient.signOut();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.signout:
                signOut();
                break;
            // action with ID action_settings was selected
            case R.id.past_trackings:
                Intent intent = new Intent(this, PastTrackingActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainActivity.mGoogleSignInClient.signOut();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        startServiceButton = (Button) findViewById(R.id.start_tracking);
        stopServiceButton = (Button) findViewById(R.id.stop_tracking);

        Intent intent = getIntent();
        Toast.makeText(getApplicationContext(),""+intent.getStringExtra("personName")+" "+intent.getStringExtra("personEmail"),Toast.LENGTH_LONG).show();
        userEmail = intent.getStringExtra("personEmail");

        String userPathTemp = "";
        String temps[] = userEmail.split("\\.");
        System.out.println("UserEmail ---"+ userEmail+" ---- "+temps[0]);
        //System.out.println("temps - "+temps);
        for(int i = 0;i<temps.length;i++){
            userPathTemp+=temps[i];
            System.out.println("temps---"+temps[i]);
        }
        userPathGlobal = userPathTemp;


        //If location services is off, request user to turn it on.
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Please enable location services", Toast.LENGTH_SHORT).show();
            finish();
        }


        //Map Fragment Setup...
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMapGlobal = googleMap;

        googleMapGlobal.setMyLocationEnabled(true);
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            googleMapGlobal.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                            googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude()))
                                    .title("Current Location"));


                            googleMapGlobal.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude())));

                            // Zoom in the Google Map
                            googleMapGlobal.animateCamera(CameraUpdateFactory.zoomTo(15));
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });


        /*
        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(-35.016, 143.321),
                        new LatLng(-34.747, 145.592),
                        new LatLng(-34.364, 147.891),
                        new LatLng(-33.501, 150.217),
                        new LatLng(-32.306, 149.248),
                        new LatLng(-32.491, 147.309)));


        // Position the map's camera near Alice Springs in the center of Australia,
        // and set the zoom factor so most of Australia shows on the screen.
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-23.684, 133.903), 4));

        */
    }

    public void startTrackerService(View view) {
        googleMapGlobal.setMyLocationEnabled(false);
        Toast.makeText(getApplicationContext(), "Starting new Tracking Session ...", Toast.LENGTH_LONG).show();

        view.setEnabled(false);
        stopServiceButton.setEnabled(true);
        serviceIntent = new Intent(this, TrackingService.class);

        //generate database path
        final String sessionId = getDate();
        final String userPath = userPathGlobal;
        System.out.println("user path ---" + userPath);
        //Current Path will be needed for live tracking.
        firebaseCurrentPath = userPath+"/"+sessionId;
        System.out.println("Firebase current sess path ---"+firebaseCurrentPath);

        startService(serviceIntent);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        System.out.println("Value Path ---"+firebaseCurrentPath);
        DatabaseReference myRef = database.getReference(firebaseCurrentPath);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                //System.out.println("key....."+dataSnapshot.getKey());
                //System.out.println("Value...."+dataSnapshot.getValue());
                locationPoints = new ArrayList<>();

                for(DataSnapshot childSnapShot: dataSnapshot.getChildren()) {
                    LocationPoint locationPoint = childSnapShot.getValue(LocationPoint.class);
                    System.out.println("Lat --"+locationPoint.getLatitude());
                    System.out.println("Long --"+locationPoint.getLongitude());
                    locationPoints.add(locationPoint);
                }
                googleMapGlobal.clear();
                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.clickable(true);
                int position = 0;
                for(LocationPoint tempLocationPoint: locationPoints){
                    polylineOptions.add(new LatLng(tempLocationPoint.getLatitude(), tempLocationPoint.getLongitude()));
                    String title = "Tracking Position";
                    if(position == 0) {
                        title = "Starting Position";
                    }
                    if(position == locationPoints.size()-1){
                        title = "Current Position";
                    }
                    googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(tempLocationPoint.getLatitude(), tempLocationPoint.getLongitude()))
                            .title(title)
                            .alpha(0)
                            .snippet("Lat - "+tempLocationPoint.getLatitude()+" Long - "+tempLocationPoint.getLongitude()+" Battery - "+tempLocationPoint.getBatteryPercentage()+"%"));

                    googleMapGlobal.addCircle(new CircleOptions()
                            .center(new LatLng(tempLocationPoint.getLatitude(), tempLocationPoint.getLongitude())).fillColor(R.color.green).strokeColor(R.color.red).radius(15).visible(true));
                    position+=1;
                }
                googleMapGlobal.addPolyline(polylineOptions);
                if(locationPoints.size()!=0) {
                    googleMapGlobal.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationPoints.get(locationPoints.size() - 1).getLatitude(), locationPoints.get(locationPoints.size() - 1).getLongitude()), 17));
                    //googleMapGlobal.addCircle(new CircleOptions().center(new LatLng(locationPoints.get(locationPoints.size() - 1).getLatitude(), locationPoints.get(locationPoints.size() - 1).getLongitude())).fillColor(R.color.colorAccent).strokeColor(R.color.colorPrimary).radius(15).visible(true));
                    googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(locationPoints.get(locationPoints.size() - 1).getLatitude(), locationPoints.get(locationPoints.size() - 1).getLongitude())));
                }
                if(locationPoints.size()!=0)
                    googleMapGlobal.addMarker(new MarkerOptions().position(new LatLng(locationPoints.get(0).getLatitude(), locationPoints.get(0).getLongitude())));
                //googleMapGlobal.addCircle(new CircleOptions().center(new LatLng(locationPoints.get(0).getLatitude(), locationPoints.get(0).getLongitude())).fillColor(R.color.colorPrimary).strokeColor(R.color.colorAccent).radius(15).visible(true));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value."+ error.toException());
            }
        });

    }

    public void stopTrackerService(View view){
        googleMapGlobal.setMyLocationEnabled(true);
        Toast.makeText(getApplicationContext(), "Stopping current tracking session...", Toast.LENGTH_LONG).show();

        view.setEnabled(false);
        startServiceButton.setEnabled(true);
        stopService(serviceIntent);

        SessionTracking sessionTracking = new SessionTracking("session_temp");
        for(LocationPoint locationPoint: locationPoints){
            sessionTracking.addLocationPoint(locationPoint);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);

        NumberFormat formatter = new DecimalFormat("#0.00");
        builder.setMessage("Total Distance Travelled - "+formatter.format(sessionTracking.getDistanceTravelled()/1000)+" km\n Average Speed - "+formatter.format(sessionTracking.getAverageSpeed())+" km/hr")
                .setTitle("Current Session Details");

        AlertDialog dialog = builder.create();
        dialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                }
                return;
            }

        }
    }
    private String getDate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy-HH:mm:ss");

        return df.format(c);
    }

}
