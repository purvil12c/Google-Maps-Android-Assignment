package purvilbambharolia.locationtracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by purvil12c on 09/03/18.
 */

public class DateTracking {

    private String Date;
    private double totalDistanceTravelled = 0;
    private double averageSpeed = -1;
    private ArrayList<Double> averageSpeeds = new ArrayList<>();
    private List<SessionTracking> sessionTrackings = new ArrayList<>();

    public DateTracking(String Date){
        this.Date = Date;
    }

    public String getDate(){
        return this.Date;
    }
    public double getTotalDistanceTravelled(){
        return this.totalDistanceTravelled;
    }
    public void setTotalDistanceTravelled(double distanceTravelled){
        this.totalDistanceTravelled = distanceTravelled;
    }
    public void addtoTotalDistanceTravelled(double distanceTravelled){
        this.totalDistanceTravelled += distanceTravelled;
    }
    public double getAverageSpeed(){
        if(this.averageSpeed!=-1)   return this.averageSpeed;

        double avg = 0.0;
        for(Double d: averageSpeeds){
            avg+=d;
        }
        avg=avg/averageSpeeds.size();
        if(Double.isNaN(avg) || Double.isInfinite(avg)){
            avg = 0;
        }
        this.averageSpeed = avg;
        return this.averageSpeed;
    }
    public void addAverageSpeed(double averageSpeed){
        this.averageSpeeds.add(averageSpeed);
    }

    public void addSessionTracking(SessionTracking sessionTracking){
        sessionTrackings.add(sessionTracking);
    }

    public List<SessionTracking> getSessionTrackings(){
        return this.sessionTrackings;
    }

}
