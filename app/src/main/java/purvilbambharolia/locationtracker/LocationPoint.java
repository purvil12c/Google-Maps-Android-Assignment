package purvilbambharolia.locationtracker;

/**
 * Created by purvil12c on 08/03/18.
 */

public class LocationPoint {

    public double latitude;
    public double longitude;
    public int batteryPercentage;
    public String timeinMS;


    public LocationPoint() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public LocationPoint(double latitude, double longitude, int batteryPercentage, String timeinMS){
        this.latitude = latitude;
        this.longitude = longitude;
        this.batteryPercentage = batteryPercentage;
        this.timeinMS = timeinMS;
    }

    public double getLatitude(){
        return this.latitude;
    }

    public double getLongitude(){
        return this.longitude;
    }

    public int getBatteryPercentage(){
        return this.batteryPercentage;
    }

    public String getTimeinMS(){
        return this.timeinMS;
    }
}
